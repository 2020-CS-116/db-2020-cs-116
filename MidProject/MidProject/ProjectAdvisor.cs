﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject
{
    public partial class ProjectAdvisor : Form
    {
        public ProjectAdvisor()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ProjectAdvisor_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select P.ProjectId,P.AdvisorId,A.FirstName,A.LastName,P.AdvisorRole,P.AssignmentDate from ProjectAdvisor As P join Person As A On A.Id=P.AdvisorId ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Advisor ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["Id"].ToString());


            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Project ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox2.Items.Add(dr["Id"].ToString());


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId, @ProjectId,@AdvisorRole, @AssignmentDate)", con);
            cmd.Parameters.AddWithValue("@AdvisorId", comboBox1.Text);
            cmd.Parameters.AddWithValue("@ProjectId", comboBox2.Text);
            cmd.Parameters.AddWithValue("@AdvisorRole", textBox1.Text);
            cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Value);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Project Advisor Added Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Adding Project Advisor");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand SqlComm = new SqlCommand("DELETE ProjectAdvisor WHERE AdvisorId=@AdvisorId and ProjectId=@ProjectId", con);
            SqlComm.Parameters.AddWithValue("@AdvisorId", comboBox1.Text);
            SqlComm.Parameters.AddWithValue("@ProjectId", comboBox2.Text);
            try
            {
                SqlComm.ExecuteNonQuery();
                MessageBox.Show("Data record deleted!");
            }
            catch (Exception x)
            {
                MessageBox.Show("Erron on Deleting Project Advisor");
            }
            finally
            {
                con.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From ProjectAdvisor Where AdvisorId=@AdvisorId", con);
            cmd.Parameters.AddWithValue("@AdvisorId", comboBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select A.Id,P.FirstName,P.LastName,A.Designation,A.Salary from Advisor as A join Person as P on A.Id=P.Id ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
