﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject
{
    public partial class Student1 : Form
    {
        public Student1()
        {
            InitializeComponent();
        }

        private void Student1_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName ,@LastName, @Contact ,@Email ,@DateOfBirth, @Gender)", con);
            cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
            cmd.Parameters.AddWithValue("@LastName", textBox5.Text);
            cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
            cmd.Parameters.AddWithValue("@Email", textBox3.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", dateTimePicker1.Value);
            cmd.Parameters.AddWithValue("@Gender", textBox2.Text);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Person Added Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Adding Person");
            }
            
            /*
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student values (@Id,@RegistrationNo)", con);
            cmd.Parameters.AddWithValue("@Id", textBox6.Text);
            cmd.Parameters.AddWithValue("@RegistrationNo", textBox7.Text);
            cmd.ExecuteNonQuery();
            /*try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Student Added Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Adding Student");
            }*/



        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Person", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand SqlComm = new SqlCommand("DELETE Person WHERE FirstName=@FirstName", con);
            SqlComm.Parameters.AddWithValue("@FirstName", textBox1.Text);
            try
            {
                SqlComm.ExecuteNonQuery();
                MessageBox.Show("Data record deleted!");
            }
            catch (Exception x)
            {
                MessageBox.Show("Erron on Deleting Student information");
            }
            finally
            {
                con.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From Person Where FirstName=@FirstName", con);
            cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
