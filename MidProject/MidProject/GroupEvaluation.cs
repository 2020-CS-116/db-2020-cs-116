﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace MidProject
{
    public partial class GroupEvaluation : Form
    {
        public GroupEvaluation()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into GroupEvaluation values (@GroupId, @EvaluationId,@ObtainedMarks, @EvaluationDate)", con);
            cmd.Parameters.AddWithValue("@GroupId", comboBox1.Text);
            cmd.Parameters.AddWithValue("@EvaluationId", comboBox2.Text);
            cmd.Parameters.AddWithValue("@ObtainedMarks", textBox1.Text);
            cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Value);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Evaluation  Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Completing Evaluation");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from [Group] ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["Id"].ToString());


            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Evaluation ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox2.Items.Add(dr["Id"].ToString());


            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select G.GroupId,G.EvaluationId,E.Name,G.ObtainedMarks,E.TotalMarks,G.EvaluationDate from GroupEvaluation AS G join Evaluation As E On G.EvaluationId=E.Id ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand SqlComm = new SqlCommand("DELETE GroupEvaluation WHERE GroupId=@GroupId and EvaluationId=@EvaluationId", con);
            SqlComm.Parameters.AddWithValue("@GroupId", comboBox1.Text);
            SqlComm.Parameters.AddWithValue("@EvaluationId", comboBox2.Text);
            try
            {
                SqlComm.ExecuteNonQuery();
                MessageBox.Show("Data record deleted!");
            }
            catch (Exception x)
            {
                MessageBox.Show("Erron on Deleting Project Advisor");
            }
            finally
            {
                con.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From GroupEvaluation Where GroupId=@GroupId", con);
            cmd.Parameters.AddWithValue("@GroupId", comboBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Evaluation ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
