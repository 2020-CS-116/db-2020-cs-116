﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MidProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public void loadform(object form)
        {
            if (this.panelmain.Controls.Count > 0)
                this.panelmain.Controls.RemoveAt(0);
            Form f = form as Form;
            f.TopLevel = false;
            f.Dock = DockStyle.Fill;
            this.panelmain.Controls.Add(f);
            this.panelmain.Tag = f;
            f.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadform(new Student1());
        }

        private void button11_Click(object sender, EventArgs e)
        {
            loadform(new Student());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            loadform(new Project());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            loadform(new Advisor());
        }

        private void button12_Click(object sender, EventArgs e)
        {
            loadform(new Group());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            loadform(new StudentEvaluation());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            loadform(new GroupStudent());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            loadform(new GroupProject());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            loadform(new ProjectAdvisor());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            loadform(new GroupEvaluation());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            loadform(new ReportGenrator());
        }
    }
}
