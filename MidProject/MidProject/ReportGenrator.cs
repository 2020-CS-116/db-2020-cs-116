﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace MidProject
{
    public partial class ReportGenrator : Form
    {
        public ReportGenrator()
        {
            InitializeComponent();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select P.ProjectId,G.GroupId, P.AdvisorId,A.FirstName,A.LastName,S.StudentId,Pa.FirstName,Pa.LastName,P.AssignmentDate from ProjectAdvisor As P join Person As A On A.Id=P.AdvisorId JOIN GroupProject as G on G.ProjectId=P.ProjectId join GroupStudent As S on S.GroupId=G.GroupId join Person as Pa on Pa.Id=S.StudentId ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select G.GroupId,Ga.StudentId, P.FirstName,P.LastName,G.EvaluationId,E.Name,G.ObtainedMarks,E.TotalMarks,E.TotalWeightage,G.EvaluationDate from GroupEvaluation AS G join Evaluation As E On G.EvaluationId=E.Id join GroupStudent AS Ga on Ga.GroupId=G.GroupId join Person As P on P.Id=Ga.StudentId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select S.Id,S.RegistrationNo,P.FirstName,P.LastName,P.Contact,P.Email,P.DateOfBirth from Student as S join Person as P on S.Id=P.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select P.ProjectId,P.AdvisorId,A.FirstName,A.LastName,A.Contact,A.Email,A.DateOfBirth,P.AdvisorRole,P.AssignmentDate from ProjectAdvisor As P join Person As A On A.Id=P.AdvisorId ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select G.ProjectId,G.GroupId,S.StudentId,P.FirstName, P.LastName,P.Contact,P.Email, G.AssignmentDate from GroupProject As G join GroupStudent As S on S.GroupId=G.GroupId join Person as P on P.Id=S.StudentId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
        }
    }
}
