﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MidProject
{
    public partial class StudentEvaluation : Form
    {
        public StudentEvaluation()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Evaluation ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void StudentEvaluation_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Evaluation values (@Name,@TotalMarks,@TotalWeightage)", con);
            cmd.Parameters.AddWithValue("@Name", textBox5.Text);
            cmd.Parameters.AddWithValue("@TotalMarks", textBox4.Text);
            cmd.Parameters.AddWithValue("@TotalWeightage", textBox3.Text);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Evaluation Added Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Adding Evaluation");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand SqlComm = new SqlCommand("DELETE Evaluation WHERE Name=@Name", con);
            SqlComm.Parameters.AddWithValue("@Name", textBox5.Text);
            try
            {
                SqlComm.ExecuteNonQuery();
                MessageBox.Show("Data record deleted!");
            }
            catch (Exception x)
            {
                MessageBox.Show("Erron on Deleting Student");
            }
            finally
            {
                con.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From Evaluation Where Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", textBox5.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
